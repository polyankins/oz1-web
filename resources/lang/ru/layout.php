<?php
return [
    'support_phone' => 'Телефон поддержки: :0',
    'more_then_products' => ':0+ Товаров!',
    'search_our_catalogue' => 'Поиск по каталогу',
    'Categories' => 'Категории',
    'Grid' => 'Сетка',
    'List' => 'Список',
    'Showing_to_of_Pages' => 'Отображается с :0 по :1 из :2 (:3 страниц)',
    'Show' => 'Показывать по',
    'Sort_By' => 'Сортировать по',
    'Default' => 'по умолчанию',
    'Discount (Highest)' => 'Скидке (от большей)',
    'Discount (Lowest)' => 'Скидке (от меньшей)',
    'Name (A - Z)' => 'Названию (от А до Я)',
    'Name (Z - A)' => 'Названию (от Я до А)',
    'Price (Low > High)' => 'Цена (от меньшей)',
    'Price (High > Low)' => 'Цена (от большей)',
    'Load more' => 'Загрузить еще',
    'Discount percentage (Highest)' => 'Процент скидки (от большего)',
    'Discount percentage (Lowest)' => 'Процент скидки (от меньшего)'
];