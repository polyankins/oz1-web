<?php
return [
    'support_phone' => 'Support: :0',
    'more_then_products' => ':0+ Products!',
    'search_our_catalogue' => 'Search Our Catalogue',
    'Categories' => 'Categories',
    'Grid' => 'Grid',
    'List' => 'List',
    'Showing_to_of_Pages' => 'Showing :0 to :1 of :2 (:3 Pages)',
    'Show' => 'Show',
    'Sort_By' => 'Sort by',
    'Default' => 'Default',
    'Discount (Highest)' => 'Discount (Highest)',
    'Discount (Lowest)' => 'Discount (Lowest)',
    'Name (A - Z)' => 'Name (A - Z)',
    'Name (Z - A)' => 'Name (Z - A)',
    'Price (Low > High)' => 'Price (Low > High)',
    'Price (High > Low)' => 'Price (High > Low)',
    'Load more' => 'Load more',
    'Discount percentage (Highest)' => 'Discount percentage (Highest)',
    'Discount percentage (Lowest)' => 'Discount percentage (Lowest)'
];