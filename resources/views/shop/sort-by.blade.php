<div class="sort-by-wrapper">
    <div class="col-md-2 text-right sort-by">
        <label class="control-label" for="input-sort">{{__('layout.Sort_By')}}:</label>
    </div>
    <div class="col-md-3 text-right sort">
        <div class="select-wrapper">
            <select id="input-sort" class="form-control" onchange="location = this.value;">

                @foreach($sorter as $num=>$sort)
                    @if (is_array($sort)) {
                    <option value="?{{http_build_query($sort['params'], '', '&')}}"
                            @if ((isset($sorter['selected']) && $sorter['selected'] == $num))
                            selected="selected"
                            @endif>{{$sort['title']}}
                    </option>
                    @endif
                @endforeach

            </select>
        </div>
    </div>
</div>