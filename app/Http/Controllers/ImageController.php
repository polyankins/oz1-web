<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ImageController extends Controller
{
    public function resize($h, $w, $img)
    {
        $file = public_path() . $img;
        $pathParts = pathinfo(parse_url($img, PHP_URL_PATH));
        $resizePath = Storage::disk('cache')->path($pathParts['dirname'] . '/');
        $resizeFile = $resizePath . $h . '_' . $w . '_' . $pathParts['basename'];
        if (!is_dir($resizePath)) {
            @mkdir($resizePath, 0777, true);
        }
        if (!is_file($resizeFile)) {
            if (!is_file($file)) {
                throw new NotFoundHttpException(' Not found file: ' . $file);
            }
            $img = Image::make($file)->resize(null, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($resizeFile);
        } else {
            $img = Image::make($resizeFile);
        }
        return $img->response($pathParts['extension']);
    }
}