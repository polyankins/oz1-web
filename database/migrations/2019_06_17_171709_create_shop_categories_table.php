<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;

class CreateShopCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('eid');
            NestedSet::columns($table);
            //$table->unsignedInteger('parent_id')->nullable(true);
            $table->foreign('parent_id')->references('id')->on('shop_categories');
            $table->string('tid',80)->nullable(true)->unique();
            $table->string('name');
            $table->timestamps();

            $table->index(['eid','parent_id'],'shc_base');
            $table->index('tid', 'shc_slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_categories');
    }
}
